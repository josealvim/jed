#pragma once 

#include <string>

namespace jed :: utf8 {

using u8str = std::string;

// count scalar values, not _really_ characters 
size_t scalar_length (u8str const&);

}
