WARNINGS = \
	-pedantic 				\
	-Wall 					\
	-Wextra 				\
	-Wcast-align 			\
	-Wcast-qual 			\
	-Wctor-dtor-privacy 	\
	-Wdisabled-optimization \
	-Wformat=2 				\
	-Winit-self 			\
	-Wlogical-op 			\
	-Wmissing-include-dirs 	\
	-Wnoexcept 				\
	-Wold-style-cast 		\
	-Woverloaded-virtual 	\
	-Wredundant-decls 		\
	-Wshadow 				\
	-Wsign-promo 			\
	-Wstrict-null-sentinel 	\
	-Wstrict-overflow=5 	\
	-Wswitch-default 		\
	-Wundef 				\
	-Werror 				\
	-Wno-unused

GPP = g++ -I. -std=c++2a -g ${WARNINGS}

SOURCES = 			\
	io/raw_raii.o	\
	io/nbio.o		\
	utf8.o			\
	text.o			\
	jed.o

all: ${SOURCES}
	${GPP} $^    -o jed.out

%.o: %.cpp
	${GPP} $^ -c -o $@

clean:
	find -type f -name "*.out" -exec rm {} \;
	find -type f -name "*.o"   -exec rm {} \;