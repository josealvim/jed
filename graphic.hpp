#pragma once 

#include <utf8.hpp>

namespace jed :: graphic {

struct SGR {
	static const int normal	= 0;
	static const int uline	= 4;
	static const int oline	= 53;
	static const int blink	= 5;
	static const int invert	= 7;
	static const int del	= 9;
};
struct TermPos {
	int row, col;
};

inline utf8::u8str hide_cursor() {
	return "\x1b[?25l";
}
inline utf8::u8str show_cursor() {
	return "\x1b[?25h";
}
inline utf8::u8str zero_cursor() {
	return "\x1b[H";
}
inline utf8::u8str move_cursor(TermPos pos) {
	return	"\x1b["
		+	std::to_string(pos.row)
		+ 	";"
		+	std::to_string(pos.col)
		+	"H";
}
inline utf8::u8str  fwd_cursor(int x) {
	return "\x1b["+std::to_string(x)+"C";
}
inline utf8::u8str back_cursor(int x) {
	return "\x1b["+std::to_string(x)+"D";
}
inline utf8::u8str   up_cursor(int x) {
	return "\x1b["+std::to_string(x)+"A";
}
inline utf8::u8str down_cursor(int x) {
	return "\x1b["+std::to_string(x)+"B";
}
inline utf8::u8str   nl_cursor(int x) {
	return "\x1b["+std::to_string(x)+"E";
}
inline utf8::u8str   pl_cursor(int x) {
	return "\x1b["+std::to_string(x)+"D";
}
inline utf8::u8str clearscreen() {
	return "\x1b[2J";
}
inline utf8::u8str rgb_fg(int r, int g, int b) {
	return 	"\x1b[38;2;" 
		+	std::to_string(r) + ";"
		+	std::to_string(g) + ";"
		+	std::to_string(b) + "m";
}
inline utf8::u8str rgb_bg(int r, int g, int b) {
	return 	"\x1b[48;2;" 
		+	std::to_string(r) + ";"
		+	std::to_string(g) + ";"
		+	std::to_string(b) + "m";
}

template <typename... Attrs>
inline utf8::u8str SGR_Attr(Attrs... attrs) {
	return "\x1b[" + ((std::to_string(attrs) + ";") + ...) + "m";
}

template <>
inline utf8::u8str SGR_Attr() {
	return "\x1b[m";
}

struct WindowSkeleton {
	struct UpperFrame {
		int ulcorner;
		int sidebar;
		int l_scroll;
		int m_frame;
		int overhang;
		int l_bullet;
		int tabwidth;
		int r_bullet;
		int r_line;
		int urcorner;
		int r_scroll;
	};
	struct MiddleFrame {
		int l_frame;
		int sidebar;
		int l_scroll;
		int m_frame;
		int linenums;
		int space;
		int text_box;
		int r_frame;
		int r_scroll;
	};
	struct LowerFrame {
		int llcorner;
		int sidebar;
		int l_scroll;
		int m_frame;
		int overhang;
		int l_bullet;
		int width;
		int r_bullet;
		int lrcorner;
		int r_scroll;
	};

	UpperFrame  u;
	MiddleFrame m;
	LowerFrame  l;
	int winheight;

	WindowSkeleton(int sidebar, int lnum, int textwidth) {
		u = {
			.ulcorner = 1,
			.sidebar  = sidebar,
			.l_scroll = 1,
			.m_frame  = 1,
			.overhang = lnum,	// │ 
			.l_bullet = 1,		// ╰→ lnum  
			.tabwidth = textwidth,
			.r_bullet = 1,
			.r_line	  = 1,
			.urcorner = 1,
			.r_scroll = 1,
		};
		m = {
			.l_frame  = 1,
			.sidebar  = sidebar,
			.l_scroll = 1,
			.m_frame  = 1,
			.linenums = lnum,
			.space    = 1,
			.text_box = textwidth,
			.r_frame  = 1,
			.r_scroll = 1,
		};
		l = {
            .llcorner = 1,
			.sidebar  = sidebar,
			.l_scroll = 1,
			.m_frame  = 1,
			.overhang = lnum,
			.l_bullet = 1,
			.width    = textwidth,
			.r_bullet = 1,
			.lrcorner = 1,
			.r_scroll = 1,
		};
		winheight = 40;
	}

	TermPos line_number_start_pos(int index) const {
		return TermPos {
			.row = 1+1 + index,	// starts from one, then skip frame
			.col = 1			// again, from one
				 +	m.l_frame
				 +	m.sidebar
				 +	m.l_scroll
				 +	m.m_frame,
		};
	}
	TermPos        line_start_pos(int index) const {
		return TermPos {
			.row = 1+1 + index,	// starts from one
			.col = 1			// again, from one
				 +	m.l_frame
				 +	m.sidebar
				 +	m.l_scroll
				 +	m.m_frame
				 +	m.linenums
				 +	m.space,
		};
	}
	TermPos  contextual_start_pos() const {
			return TermPos {
				.row = 1+1 + winheight,	// starts from one
				.col = 1				// again, from one
					+	l.llcorner
					+	l.sidebar
					+	l.l_scroll
					+	l.m_frame
					+	l.overhang
					+	l.l_bullet,
			};
		}
	
	utf8::u8str frame_str() const {
		utf8::u8str result = zero_cursor();

		{	utf8::u8str upper_frame;
			upper_frame += "╭";						// ulcorner
			for (auto i = 0; i < u.sidebar; i++)	// sidebar
				upper_frame += "─";
			upper_frame += "─";						// l_scroll
			upper_frame += "┬";						// m_frame
			for (auto i = 1; i < u.overhang; i++)	// overhang
				upper_frame += "─";
			upper_frame += "╸";						// l_bullet
			upper_frame += fwd_cursor(u.tabwidth);	// tabwidth
			upper_frame += "╺";						// urcorner
			upper_frame += "╮";						// r_scroll
			
			result += upper_frame;
			result += nl_cursor(1);
		}
		{	utf8::u8str middle_cols;
			middle_cols += "│";						// l_frame
			middle_cols += fwd_cursor(m.sidebar);	// sidebar
			middle_cols += " ";						// l_scroll
			middle_cols += "│"; 					// m_frame
			middle_cols += fwd_cursor(m.linenums);	// linenums
			middle_cols += " ";						// space
			middle_cols += fwd_cursor(m.text_box);	// text_box
			middle_cols += "│"; 					// r_frame
			middle_cols += " ";						// r_scroll

			for (int i = 0; i < winheight; i++) {
				result += middle_cols;
				result += nl_cursor(1);
			}
		}
		{	utf8::u8str lower_frame;
			lower_frame += "╰";						// llcorner
			for (auto i = 0; i < l.sidebar; i++)	// sidebar
				lower_frame += "─";
			lower_frame += "─";						// l_scroll
			lower_frame += "┴";						// m_frame
			for (auto i = 1; i < l.overhang; i++)	// overhang
				lower_frame += "─";
			lower_frame += "╸";						// l_bullet
			for (auto i = 0; i < l.width; i++)		// width
				lower_frame += " ";
			lower_frame += "╺";						// r_bullet
			lower_frame += "╯";						// lrcorner
			lower_frame += " ";						// r_scroll
			result += lower_frame;
		}
		
		return result;
	}
};

}