# Contributing
Hello, thank you for considering contributing to this 
project! I haven't received contributions before, so 
this would be a first.

## Copyright and Legal Stuff
 * As is standard, you retain ownership and copyright of any 
   code that you created (whether you explicitly claim it or not).
 * Copyright is, not attributed to the project unless explicitly 
   requested by the contributor. Should you wish to clarify the 
   copyright of a piece of code, you may add a copyright notice 
   in the header or source files you've contributed.
 * All contributed code is to be available under the terms of the
   GPLv3 license.
 * I remain available and will act in good faith to rectify any 
   potential misuses and misattributions of copyright. However, I 
   rely on contributors to _do the right thing_ ™. 
