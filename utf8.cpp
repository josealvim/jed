#include <utf8.hpp>

namespace jed :: utf8 {

size_t scalar_length (u8str const& string) {
	size_t i   = 0;
	size_t len = 0;

	while (i < string.size()) {
		auto c = string[i];
		len += 1;	

		// rfc3629 section-3
		if ((c & 0b11111000) == 0b11110000)
			i += 4;
		else 
		if ((c & 0b11110000) == 0b11100000)
			i += 3;
		else 
		if ((c & 0b11100000) == 0b11000000)
			i += 2;
		else
		if ((c & 0b10000000) == 0b00000000)
			i += 1;
	}

	return len;
}

}