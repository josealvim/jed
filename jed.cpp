#include <io/raw_raii.hpp>
#include <io/nbio.hpp>

#include <graphic.hpp>
#include <utf8.hpp>
#include <text.hpp>

#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <functional>
#include <iomanip>

using namespace jed::io;
using namespace jed::graphic;
using namespace jed::utf8;
using namespace jed::text;

u8str process_string(u8str const& str) {
	u8str processed;
	for (auto c : str) 
	switch (c) {
		case '\t': 
			processed += "    ";
			break;
		default:
			processed += u8str(&c, 1);
			break;
	}

	return processed;
}

struct EditorState {
	struct StrPair {
		u8str file;
		u8str show;
	};

	WindowSkeleton skel;
	TermPos cursor;

	int row_offset;
	
	std::vector<StrPair> rows;
	u8str contextual_msg;

	bool exiting;

	EditorState(
		WindowSkeleton&& s,
		std::vector<u8str>  file_data
	): skel(std::move(s)), cursor({0,0}), row_offset(0) {
		exiting = false;
		contextual_msg = 
			rgb_fg(128, 128, 128) +
			"janky editor ─ version -∞" +
			SGR_Attr();

		rows.reserve(file_data.size());
		for (auto && line : file_data) {
			rows.push_back(
				{line, process_string(line)}
			);
		}
	}

	int selected_line() {
		int candidate = row_offset + cursor.row;
		if (candidate < 0 || candidate >= static_cast<int>(rows.size()))
			return -1;
		else
			return candidate;
	}

	int selected_line_length() {
		auto line = selected_line();
		if ( line < 0 )
			return -1;
		return scalar_length(rows[line].show);
	}

	u8str linenums_str() const {
		u8str result;
		for (int i = 0; i < skel.winheight; i++) {
			auto pos  = skel.line_number_start_pos(i);
			auto line = static_cast<size_t>(i + row_offset);
			auto num = std::to_string(line);

			result += jed::graphic::move_cursor(pos);
			if ( i + row_offset < 0) {
				result += u8str(skel.m.linenums - 1, ' ');
				result += "~";
			} else 
			if ( line < rows.size() ) {
				result += u8str(skel.m.linenums - num.size(), ' ');
				result += num;
			} else {
				result += u8str(skel.m.linenums - 1, ' ');
				result += "~";
			}
		}
		return result;
	}

	u8str text_str() const {
		u8str result;
		for (auto i = 0; i < skel.winheight; i++) {
			result += move_cursor(skel.line_start_pos(i));
			size_t padlen;

			if ( i + row_offset < 0)
				padlen = static_cast<size_t>(skel.m.text_box);
			else 
			if ( static_cast<size_t>(i + row_offset) < rows.size() ) {
				auto const &show = rows[i + row_offset].show;
				result += show;
				padlen = static_cast<size_t>(
					skel.m.text_box - scalar_length(show)
				);
			} else {
				padlen = static_cast<size_t>(skel.m.text_box);
			}

			result += padding(" ", padlen);
		}
		return result;
	}

	u8str context_str() const {
		u8str result;
		result += move_cursor(skel.contextual_start_pos());
		result += contextual_msg;
		return result;
	}
};

auto read_file (u8str const& path) {
	std::vector<u8str> result;
	auto ifile = std::ifstream(path);
	
	u8str line;
	while (std::getline(ifile, line)) {
		result.emplace_back(std::move(line));
		line.clear();
	}

	return result;
}

using control_func_t = 
	std::function<void(std::string const&, EditorState&)>;

struct Control: control_func_t {
	using control_func_t::function;
	Control operator* (Control const& C) const {
		return [a = *this, b = C]
		(std::string const& seq, EditorState& state) {
			a(seq, state);
			b(seq, state);
		};
	}
};

int main () {
	Control main_control =
	Control (	// unconstrained cursor movement
		[](std::string const & seq, EditorState &state) {
			auto &cursor = state.cursor;
			
			if	(seq == "\x1b\x5b\x41") 
				cursor.row--;
			if	(seq == "\x1b\x5b\x42") 
				cursor.row++;
			if	(seq == "\x1b\x5b\x43") 
				cursor.col++;
			if	(seq == "\x1b\x5b\x44") 
				cursor.col--;
		}
	) * 
	Control (	// cursor movement over line end
		[](std::string const & seq, EditorState &state) {
			auto & cursor = state.cursor;
			if (state.selected_line() < 0)
				return;

			if (
				cursor.col > 
				state.selected_line_length()+1
			) {
				cursor.col = state.selected_line_length();
				
			}

			if (
				cursor.col > 
				state.selected_line_length()
			) {
				cursor.col = 0;
				cursor.row++;
			}

			if (cursor.col < 0) {
				cursor.row--;
				cursor.col = state.selected_line_length();
			}
		}
	) *
	Control (	// handle scrolling and cursor bounds
		[](std::string const & seq, EditorState &state) {
			auto &cursor = state.cursor;
			if (cursor.row < 0) {
				state.row_offset--;
				cursor.row++;
			}
			
			if (cursor.row >= state.skel.winheight) {
				state.row_offset++;
				cursor.row--;
			}

			if (cursor.col < 0) {
				cursor.col++;
			}
			
			if (cursor.col >= state.skel.m.text_box) {
				cursor.col--;
			}
		}
	) * 
	Control (	// trigger exiting on pressing C-a
		[](std::string const & seq, EditorState &state) {
			if (seq == "\1")
				state.exiting = true;
		}
	) *
	Control (	// home, end, pgup, pgdown keys
		[](std::string const & seq, EditorState &state) {
			if (seq == "\x1b\x5b\x46")
				state.cursor.col = state.selected_line_length();
			if (seq == "\x1b\x5b\x48")
				state.cursor.col = 0;
			if (seq == "\x1b\x5b\x36\x7e")
				state.row_offset += state.skel.winheight;
			if (seq == "\x1b\x5b\x35\x7e")
				state.row_offset -= state.skel.winheight;
		}
	) *
	Control (	// debug echo sequences 
		[](std::string const & seq, EditorState &state) {
			if (seq == "") return;
			u8str sequence;
			for (auto c : seq) {
				std::stringstream ss;
				ss	<< std::hex 
					<< "\\x"
					<< static_cast<int>(c);
				sequence += ss.str();
			}

			state.contextual_msg = sequence;
		}
	);

	RAII_RawMode rmode;
	EditorState state (
		WindowSkeleton (20, 3, 80),
		read_file("jed.cpp")
	);

	(std::cout <<"\x1b[2J").flush();

	bool first = true;
	state.row_offset = -5;
	while (1) {
		auto seq = nb_read_seq(rmode);
		if ( seq == "" && ! first ) {
			continue;
		} else {
			first = false;
		}

		main_control(seq, state);
		
		u8str wbuffer;

		wbuffer += state.skel.frame_str();
		wbuffer += state.linenums_str();
		wbuffer += state.text_str();
		wbuffer += state.context_str();

		auto pos = state.skel.line_start_pos(state.cursor.row);
		pos.col += state.cursor.col;

		std::cout 
			<< hide_cursor()
			<< zero_cursor()
			<< wbuffer
			<< show_cursor()
			<< move_cursor(pos);

		std::cout.flush();

		if (seq == "\1")
			break;
	}

	std::cout 
		<< "\x1b[H"
		<< "\x1b[2J";
	std::cout.flush();

	return 0;
}
