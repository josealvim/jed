#include <text.hpp>

using jed::utf8::u8str;

namespace jed :: text {

u8str padding(u8str const &pad, size_t repeat) {
	u8str str;
	str.reserve(pad.size() * repeat);
	for (size_t i = 0; i < repeat; i++)
		str += pad;
	return str;
}

}