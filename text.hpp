#pragma once 

#include <utf8.hpp>

namespace jed :: text {

utf8::u8str padding(utf8::u8str const &pad, size_t repeat);

}