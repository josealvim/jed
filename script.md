# Raw Mode I/O

## Entering Raw Mode
```C++
	#include <termios.h>
	#include <unistd.h>
	#include <stdio.h>
	#include <stdlib.h>

	// store the original config somewhere
	tcgetattr(STDIN_FILENO, &original);
	
	// create rawmode settings
	termios rawmode = original;
	rawmode.c_lflag    &= ~(ECHO | ICANON | ISIG | IEXTEN);
	rawmode.c_iflag    &= ~(IXON | ICRNL  | BRKINT | INPCK | ISTRIP);	
	rawmode.c_oflag    &= ~(OPOST);
	rawmode.c_cflag    |=  (CS8);
	rawmode.c_cc[VMIN]  = 0;
	rawmode.c_cc[VTIME] = 1;
	
	// apply settings
	tcsetattr(STDIN_FILENO, TCSAFLUSH, &rawmode);

	/*	
		BRKINT: Break conditions cause an interrupt.
			https://www.cmrr.umn.edu/%7Estrupp/serial.html#2_3_3
			> Normally a receive or transmit data signal stays 
			> at the mark voltage until a new character is 
			> transferred. If the signal is dropped to the space 
			> voltage for a long period of time, usually 1/4 to 
			> 1/2 second, then a break condition is said to 
			> exist.

		INPCK : parity checking

		ISTRIP: 8th bit of input byte set to 0, hence breaking 
				utf8 encoding :)

		CS8   : set input size to be 8 bytes long, should 
				already be set in normal systems nowadays.

		ECHO  : Echo input characters.

		ICANON: enables canonical mode,  which, apparently
				is reading input line-by-line as opposed to 
				byte-by-byte.

		ISIG  : When any of the characters INTR, QUIT, SUSP, or 
				DSUSP are received, generate the corresponding 
				signal.	That is, we can disable C-c, C-z and 
				similar signals.

		IEXTEN : Enable implementation-defined input processing.
				 On some systems, C-v does weird stuff.

		IXON   : Enable XON/XOFF flow control on output. This 
				 corresponds to heeding to C-s and C-q input 
				 flow.

		ICRNL  : Translates carriage return to newline on input. 
			
		OPOST  : Enable implementation-defined output 
				 processing.

		c_cc[NCCS]	is an array of things called 
					“special characters”.
		c_cc[VMIN]: Minimum number of characters for 
					noncanonical read.
		c_cc[VTIME] Timeout in deciseconds for noncanonical 
					read.

		VTIME  Timeout in deciseconds for noncanonical read.
		*/
```
After this, the console will read byte-by-byte, and special 
sequences won't trigger interrupts and reading will be 
non-blocking; as well as some other misc. side effects that 
aren't properly important for us.

## Escape Sequences 

```C++
	"\x1b[2J"   ─ clear screen
	"\x1b[H"    ─ cursor home
	"\x1b[_B"   ─ cursor forward 
	"\x1b[_C"   ─ cursor down
	"\x1b[_;_H" ─ cursor to “row;col”
	"\x1b[6n"   ─ cursor position
		→ "\x1b[_;_R" 		“row;col”
	"\x1b[?25l" ─ set   cursor hidden
	"\x1b[?25h" ─ reset cursor hidden
	"\x1b[K"	─ clear single line
```

## Window Size 
```C++
	#include <sys/ioctl.h>
	winsize ws;
  	ioctl(STDOUT_FILENO, TIOCGWINSZ, &ws);
```

## Input Sequences

```C++
	"\x1b\x5b\x41"			─ ↑ 
	"\x1b\x5b\x42"			─ ↓ 
	"\x1b\x5b\x43"			─ → 
	"\x1b\x5b\x44"			─ ← 

	"\1b\5b\31\3b\35\41"	─ C-↑ 
	"\1b\5b\31\3b\35\42"	─ C-↓ 
	"\1b\5b\31\3b\35\43"	─ C-→ 
	"\1b\5b\31\3b\35\44"	─ C-← 

	"\1b\5b\31\3b\33\41"	─ A-↑ 
	"\1b\5b\31\3b\33\42"	─ A-↓ 
	"\1b\5b\31\3b\33\43"	─ A-→ 
	"\1b\5b\31\3b\33\44"	─ A-←

	"\1b\5b\32\7e"			─ INS
	"\1b\5b\33\7e"			─ DEL
	"\1b\5b\35\7e"			─ PGU
	"\1b\5b\36\7e"			─ PGD
	"\1b\5b\48"				─ HOME
	"\1b\5b\46"				─ END
	"\7f"					─ BACKSPACE

	'A' & 0x1f				─ C-a // ignores shift :l
	"\1b" "a"				─ A-a 
```

# Viewing Files/Directories

# Side Bar

# Editing Files

# Searching

# Bonuses