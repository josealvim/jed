# `JED`, the Janky Editor

## Why?
I wanted a lightweight and stupid simple editor for
my `ssh` sessions that supported multiple file tabs 
and browsing the filesystem. I  wanted something  I 
could quickly  compile  anywhere  with a modern C++ 
compiler and not something  I'd have to set up with 
plugins or that requires root to install, etc.

## Contribution
Contributions are more than welcome, 
_I have no idea_ what I am doing :)