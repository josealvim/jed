#pragma once 

#include <string>

struct termios;

namespace jed :: io {

struct RAII_RawMode {
	RAII_RawMode();
	~RAII_RawMode(); 

	termios* original;
};

}