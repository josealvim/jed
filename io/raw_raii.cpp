#include <io/raw_raii.hpp>

#include <termios.h>
#include <unistd.h>

namespace jed :: io {

RAII_RawMode::RAII_RawMode(): original(new termios) {
	// store original settings
	tcgetattr(STDIN_FILENO, original);

	// create rawmode settings
	termios rawmode = *original;
	rawmode.c_lflag    &= ~(ECHO | ICANON | ISIG | IEXTEN);
	rawmode.c_iflag    &= ~(IXON | ICRNL  | BRKINT | INPCK | ISTRIP);	
	rawmode.c_oflag    &= ~(OPOST);
	rawmode.c_cflag    |=  (CS8);
	rawmode.c_cc[VMIN]  = 0;
	rawmode.c_cc[VTIME] = 1;

	// apply settings
	tcsetattr(STDIN_FILENO, TCSAFLUSH, &rawmode);
}

RAII_RawMode::~RAII_RawMode() {
	tcsetattr(STDIN_FILENO, TCSAFLUSH, original);
	delete original;
}

}
