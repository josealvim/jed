#pragma once

#include <io/raw_raii.hpp>
#include <string>

namespace jed :: io {

std::string nb_read_seq(RAII_RawMode const&);

}