#include <io/nbio.hpp>

#include <unistd.h>
#include <sys/ioctl.h>

namespace jed :: io {

std::string nb_read_seq(RAII_RawMode const& rm) {
	int available = 0;
	ioctl(STDIN_FILENO, FIONREAD, &available);
	std::string seq;

	char *buff = new char[available];
	
	read(STDIN_FILENO, buff, available);
	
	seq = std::string(buff, available);
	
	delete[] buff;

	return seq;
}

}